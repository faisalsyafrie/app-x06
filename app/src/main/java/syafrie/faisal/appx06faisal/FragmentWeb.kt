package syafrie.faisal.appx06faisal

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kali.*
import kotlinx.android.synthetic.main.frag_data_kali.view.*
import java.text.DecimalFormat

class FragmentWeb: Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = editText2.text.toString().toDouble()
        y = editText3.text.toString().toDouble()
        hasil = x*y
        textView2.text = DecimalFormat("#.##").format(hasil)
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var weburi="https://polinema.ac.id"
        var intentinternet = Intent(Intent.ACTION_VIEW, Uri.parse(weburi))
        startActivity(intentinternet)

    }



    override fun onStart() {
        super.onStart()

    }

}