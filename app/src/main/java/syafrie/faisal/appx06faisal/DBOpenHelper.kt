package syafrie.faisal.appx06faisal

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.Context

class DBOpenHelper(context : Context) : SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text primary key, nama text not null, id_prodi int not null)"
        val tProdi = "create table prodi(id_prodi integer primary key autoincrement, nama_prodi text not null)"
        val tMatkul = "create table matkul(kode_matkul text primary key, nama_matkul text not null, id_prodi int not null)"
        val tNilai = "create table nilai(id_nilai integer primary key autoincrement, nama_mahasiswa text not null, nama_matkul text not null, nilai int not null )"
        val insProdi = "insert into prodi(nama_prodi) values('TI'),('AK'),('ME')"

        db?.execSQL(tMhs)
        db?.execSQL(tProdi)
        db?.execSQL(insProdi)
        db?.execSQL(tMatkul)
        db?.execSQL(tNilai)


    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "mahasiswa"
        val DB_Ver = 1
    }

}